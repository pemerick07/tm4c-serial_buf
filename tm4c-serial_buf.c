/*
 * Manages serial data transmission using the DMA to prevent blocking of application
 * based on serial transmission rate. Most serial interfaces are relatively slow
 * compared to code execution speed so, the application could have to wait for the
 * peripheral FIFO to be emptied or implement non-blocking data management
 * at the application level. This library gets around that limitation by writing
 * data to one of two buffers while emptying the other buffer to the peripheral FIFO
 * using DMA.
 *
 * Features:
 * -Time base of timing functions is uart frame rate (baud / bits per frame)
 * -Buffer swap timeout is used to ensure smaller segments of consecutive bytes are
 *  transmitted in timely manner
 *  -ex. 4 bytes per 2mS with 64 byte buffer would send chunks of 64 bytes
 *  @ 32mS rate if timeout wasn't used
 *      -PNG images included in project demonstrate this concept
 * -DMA transfer timeout to prevent any hangups from stopping transfers
 *  -Timeout period is # bytes to transfer + 4 bytes and because timing is based
 *  on uart frame rate, a transfer without problems shouldn't take longer than this
 *  -In case of timeout, buffer being emptied by DMA could be lost but, most likely
 *  cause of DMA timeout is interruption of a call to DMA completion interrupt
 * -If both buffers are full new data is rejected but, blocking never occurs
 *  -If data is rejected, sTX will return false
 *
 * Streaming of data is supported at speeds based on:
 * data per second must be < (baud / bits)
 * ex. 29 bytes per 2mS = 14500 bytes per S which is > (115.2kbaud / 10 bits = 11520 bytes)
 *
 */
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_udma.h"
#include "driverlib/interrupt.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"

#include "tm4c-serial_buf.h"

static sConfig_t *_config;

bool bTXIdleTO = false;

void sInit(sConfig_t *config)
{
    _config = config;

    for(uint8_t ui8I = 0; ui8I < _config->channelsSize; ui8I++)
    {
        MAP_SysCtlPeripheralEnable(_config->channels[ui8I].uartPeriph);
        MAP_UARTConfigSetExpClk(_config->channels[ui8I].uartBase, MAP_SysCtlClockGet(),
                                _config->channels[ui8I].uartBaud,
                                _config->channels[ui8I].uartDataFormat);

        // Enable FIFO and set trigger levels
        MAP_UARTFIFOEnable(_config->channels[ui8I].uartBase);
        MAP_UARTFIFOLevelSet(_config->channels[ui8I].uartBase, UART_FIFO_TX4_8, UART_FIFO_RX4_8);

        // Enable DMA for TX only
        MAP_UARTDMAEnable(_config->channels[ui8I].uartBase, UART_DMA_TX);

        // Configure TX DMA channel to accept burst requests
        MAP_uDMAChannelAttributeEnable(_config->channels[ui8I].uartDMATxCh, UDMA_ATTR_USEBURST);

        MAP_uDMAChannelControlSet(_config->channels[ui8I].uartDMATxCh | UDMA_PRI_SELECT,
                                  UDMA_SIZE_8 | UDMA_SRC_INC_8 |
                                  UDMA_DST_INC_NONE | UDMA_ARB_4);

        // Enable peripheral interrupt vector so DMA interrupts are triggered by NVIC
        MAP_IntEnable(_config->channels[ui8I].uartIntVect);

        // Setup timers
        MAP_SysCtlPeripheralEnable(_config->timerPeriph);
        MAP_TimerDisable(_config->timerBase, _config->timerHalf);
        MAP_TimerConfigure(_config->timerBase, TIMER_CFG_SPLIT_PAIR |
                           (_config->timerHalf == TIMER_A ? TIMER_CFG_A_PERIODIC : TIMER_CFG_B_PERIODIC));
        MAP_TimerControlStall(_config->timerBase, _config->timerHalf, true); // stall timer on debugger processor halt
        // Set timer to uart frame rate (based on 1 start, 1 stop, 8 data bits)
        uint32_t ui32Freq = MAP_SysCtlClockGet() / (_config->channels[ui8I].uartBaud / 10);
        MAP_TimerLoadSet(_config->timerBase, _config->timerHalf, ui32Freq);
        MAP_TimerIntEnable(_config->timerBase,
                           (_config->timerHalf == TIMER_A ? TIMER_TIMA_TIMEOUT : TIMER_TIMB_TIMEOUT));

        MAP_IntEnable(_config->timerIntVect);

        MAP_UARTEnable(_config->channels[ui8I].uartBase);
        MAP_TimerEnable(_config->timerBase, _config->timerHalf);

        // Initialize buffer management channel members
        _config->channels[ui8I].writeBuf = _config->channels[ui8I].txBufA;
        _config->channels[ui8I].writeHead = 0;
        _config->channels[ui8I].dmaRdy = true;
        _config->channels[ui8I].txIdleCntr = _config->channels[ui8I].txIdleCntrLoad;
    }
}

/*
 * Function blocks when both buffers are busy
 */
bool sTX(sCh_t *ch, uint8_t data)
{
    bool bStat = false;

    // If buffer is full
    if(ch->writeHead == ch->txBufSize)
        _sSwapBuf(ch, true); // swap buffers
    // At this point we know buffer isn't full because if it was,
    // call to sEmptyBuf would have switched to empty buffer
    else
    {
        ch->writeBuf[ch->writeHead++] = data; // save data and increment head
        bStat = true;
    }

    ch->txIdleCntr = ch->txIdleCntrLoad; // Reset idle counter

    return bStat;
}

void _sSwapBuf(sCh_t *ch, bool bBlocking)
{
    // Timeout swap shouldn't block so, skip blocking if txIdleCntr expires
    //if(bBlocking)
        // If empty buffer isn't ready it means DMA xfer isn't done
        // Block until DMA xfer is done Flag is cleared by interrupt
        //while(!ch->dmaRdy);
        //while((HWREG(UDMA_CHIS) >> ch->uartDMATxCh) == 0);
        /*{
            if((HWREG(UDMA_CHIS) >> ch->uartDMATxCh) == 1)
                ch->dmaRdy = true;
        }*/

    // If blocking is disabled, blocking won't occur so make sure buffer is ready
    // otherwise this block will be skipped
    if(ch->dmaRdy)
    {
        // If write buf is pointing to buf A
        if(ch->writeBuf == ch->txBufA)
        {
            // Setup DMA to empty bufA
            MAP_uDMAChannelTransferSet(ch->uartDMATxCh | UDMA_PRI_SELECT,
                                      UDMA_MODE_BASIC, ch->txBufA,
                                      (void *)(ch->uartBase + UART_O_DR), ch->writeHead);
            MAP_uDMAChannelEnable(ch->uartDMATxCh); // Enable channel to start xfer

            ch->writeBuf = ch->txBufB; // point to buf B
        }
        // Otherwise write buf is pointing to buf B
        else
        {
            // Setup DMA to empty bufA
            MAP_uDMAChannelTransferSet(ch->uartDMATxCh | UDMA_PRI_SELECT,
                                      UDMA_MODE_BASIC, ch->txBufB,
                                      (void *)(ch->uartBase + UART_O_DR), ch->writeHead);
            MAP_uDMAChannelEnable(ch->uartDMATxCh); // Enable channel to start xfer

            ch->writeBuf = ch->txBufA; // point to buf A
        }

        ch->dmaRdy = false; // clear DMA done flag to indicate DMA is already being used
        // Set dmaRdy timeout counter to bytes to transfer + 4
        // Timeout time base is set to UART frame rate so, transfer shouldn't take longer
        // than bytes to transfer plus a little extra
        ch->dmaRdyCntr = ch->writeHead + 4;
        ch->writeHead = 0; // reset write buffer position
    }
    else
    {
        if(ch->dmaRdyCntr > 0)
            ch->dmaRdyCntr--;
        /* When counter expires, DMA must be reset because something went wrong.
         * Disabling the DMA channel and setting the channel as ready causes
         * the next call to this function to swap the buffers which, throws away
         * the data in the buffer associated with the failed DMA transfer
         */
        else if(ch->dmaRdyCntr == 0)
        {
            MAP_uDMAChannelDisable(ch->uartDMATxCh); // Disable DMA channel
            ch->dmaRdy = true; // Set DMA channel as ready
        }
    }
}

/*
 * This function is called from the main program when an interrupt occurs.
 * No interrupt servicing is done in the main program so, this function
 * must handle clearing of the interrupt
 *
 * Parameters:
 * *ch is a pointer to the channel configuration structure
 */
void sUARTHandler(sCh_t *ch)
{
    // Check if bit associated with DMA TX channel is cleared
    // If 0, channels is disabled which means xfer completed
    // uDMAChannelIsEnabled could be used here but, this line of code uses 5 asm lines less
    if((HWREG(UDMA_ENASET) >> ch->uartDMATxCh) == 0)
        ch->dmaRdy = true; // Set empty buf as ready
}

void sTmrHandler(void)
{
    uint32_t ui32Status = MAP_TimerIntStatus(_config->timerBase, true);
    MAP_TimerIntClear(_config->timerBase, ui32Status);

    bTXIdleTO = true;
}

void sUpdt(void)
{
    if(bTXIdleTO)
    {
        bTXIdleTO = false; // Clear flag

        for(uint8_t ui8I = 0; ui8I < _config->channelsSize; ui8I++)
        {
            if(_config->channels[ui8I].txIdleCntr > 0)
                _config->channels[ui8I].txIdleCntr--;
            // If timeout occurs and data is in the buffer
            else if(_config->channels[ui8I].txIdleCntr == 0 &&
                    _config->channels[ui8I].writeHead > 0)
                _sSwapBuf(&(_config->channels[ui8I]), false);
        }
    }
}
