/*
      NOTE 1: serial_buf requires pin configuration outside of library
      NOTE 2: All Interupts are configured at build-time inside startup file
      NOTE 3: Change C Dialect to C99 by going to Right-clock project -> Properties ->
              Build -> ARM Compiler -> Advanced Options -> Language Options
      NOTE 4: Library was made for TM4C123 series so, some small adjustments may need
              to be made for TM4C129 series
*/
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_gpio.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"

#include "tm4c-serial_buf.h"

static volatile uint32_t ui32IdleClocksCnt = 0;
//static volatile uint32_t ui32IdleClocksAvg = 0;
//static volatile float fCPUIdleAvg = 0;
static volatile bool bFlgSysTick = false;

// Declare global variable for DMA
#pragma DATA_ALIGN(g_ui8DMAControlTable, 1024)
uint8_t g_ui8DMAControlTable[1024];

/************ Define serial variables *************/
// Setup buffer A and B
uint8_t rs232TxBufA[64];
uint8_t rs232TxBufB[64];

// Setup one serial channel - others can be configured
sCh_t rs232Ch =
{
    uartPeriph: SYSCTL_PERIPH_UART1,
    uartBase: UART1_BASE,
    uartBaud: 115200,
    uartIntVect: INT_UART1,
    uartDataFormat: (UART_CONFIG_WLEN_8 | UART_CONFIG_PAR_NONE | UART_CONFIG_STOP_ONE),
    uartDMATxCh: UDMA_CHANNEL_UART1TX,
    txBufA: &rs232TxBufA[0],
    txBufB: &rs232TxBufB[0],
    txBufSize: 64,
    txIdleCntrLoad: 5
};

// Declare array of pointers for serial channels
sCh_t *uartChs[1];

// Setup serial library
sConfig_t uartConfig =
{
    channelsSize: 1,
    timerPeriph: SYSCTL_PERIPH_TIMER1,
    timerBase: TIMER1_BASE,
    timerHalf: TIMER_A,
    timerIntVect: INT_TIMER1A
};

void SysTickHandler(void)
{
  /* Measure CPU Usage */
  //fCPUIdleAvg = ((float)ui32IdleClocksCnt) / 1200000;
  //ui32IdleClocksAvg = (ui32IdleClocksAvg + ui32IdleClocksCnt) / 2;
  //ui32IdleClocksCnt = 0;

    bFlgSysTick = true;
}

/* Setup interrupt handlers for tm4c-serial_buf
 *
 * Functions must be added to NVIC table in startup file
 * -UART1Handler
 * -DMAErrHandler
 * -sTmrHandler - must be added to timer that was configured in sConfig_t
 */
void UART1Handler(void)
{
    sUARTHandler(&rs232Ch);
}

void DMAErrHandler(void)
{
    uint32_t ui32Status = MAP_uDMAErrorStatusGet();
}

int main()
{
    /* Configure MCU Clock */
    // Run from the PLL at 80Mhz
    MAP_SysCtlClockSet(SYSCTL_XTAL_16MHZ |
              SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
              SYSCTL_SYSDIV_2_5);

    MAP_IntMasterDisable();

    /* Configure DMA */
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
    MAP_uDMAEnable();
    MAP_uDMAControlBaseSet(g_ui8DMAControlTable);

    /* Configure system tick */
    MAP_SysTickPeriodSet(80000000 / 500);
    MAP_SysTickIntEnable();

    //
    // Configure the GPIO Pin Mux for PB0
    // for U1RX
    //
    MAP_GPIOPinConfigure(GPIO_PB0_U1RX);
    MAP_GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0);

    //
    // Configure the GPIO Pin Mux for PB1
    // for U1TX
    //
    MAP_GPIOPinConfigure(GPIO_PB1_U1TX);
    MAP_GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_1);

    ///////////////// Configure UART //////////////////
    uartChs[0] = &rs232Ch;
    uartConfig.channels = uartChs[0];
    sInit(&uartConfig);

    MAP_IntMasterEnable();

    MAP_SysTickEnable();

    /* Infinite loop to process non-critical work */
    while(1)
    {
        sUpdt();

        if(bFlgSysTick)
        {
            bFlgSysTick = false;


            sTX(&rs232Ch, 't');
            sTX(&rs232Ch, 'e');
            sTX(&rs232Ch, 's');
            sTX(&rs232Ch, 't');
            sTX(&rs232Ch, 'i');
            sTX(&rs232Ch, 'n');
            sTX(&rs232Ch, 'g');
        }

        /* Poll uart channels for available characters */
        /*if(MAP_UARTCharsAvail(rs232Ch.uartBase))
        {
            gbSIOMsgStdUnpack((uint8_t)MAP_UARTCharGet(rs232Ch.uartBase));
        }

        if(MAP_UARTCharAvail(gpsCh.uartBase))
        {
            gbSIOMsgStdUnpack((uint8_t)MAP_UARTCharGet(rs232Ch.uartBase));
        }*/
    }
}



