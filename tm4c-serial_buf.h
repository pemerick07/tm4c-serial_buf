#ifndef TM4C_SERIAL_BUF_H_
#define TM4C_SERIAL_BUF_H_

typedef struct
{
    uint32_t uartPeriph; // uart peripheral address
    uint32_t uartBase; // uart base address
    uint32_t uartBaud; // uart baud rate
    uint32_t uartDataFormat;
    uint32_t uartIntVect;
    uint32_t uartDMATxCh;
    uint8_t *txBufA; // pointer to a byte array
    uint8_t *txBufB; // pointer to byte array
    uint16_t txBufSize; // size of array pointed to by bufA and bufB - size of buffers must be ==
    uint8_t *writeBuf;
    uint8_t writeHead;
    volatile uint8_t dmaRdy;
    uint8_t txIdleCntr;
    uint8_t txIdleCntrLoad; // Based on uart frame rate time base
    uint8_t dmaRdyCntr;
} sCh_t;

typedef struct
{
    sCh_t *channels;
    uint8_t channelsSize; // size of array pointed to by *channels
    uint32_t timerPeriph;
    uint32_t timerBase;
    uint32_t timerHalf;
    uint32_t timerIntVect;
} sConfig_t;

extern void sInit(sConfig_t *config);
extern bool sTX(sCh_t *ch, uint8_t data);
extern void sUARTHandler(sCh_t *ch);
extern void sTmrHandler(void);
extern void sUpdt(void);

static void _sSwapBuf(sCh_t *ch, bool bBlocking);



#endif /* TM4C_SERIAL_BUF_H_ */
